<?php

namespace Yarsha\MediaManagerBundle\Service;

use Yarsha\MediaManagerBundle\Entity\Post;
use Yarsha\MediaManagerBundle\Entity\Media;
use Yarsha\MediaManagerBundle\Entity\Gallery;
use Intervention\Image\ImageManager as Image;
use Symfony\Component\DependencyInjection\Container;

class MediaService
{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getMediaById($id){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $media = $em->getRepository(Media::class)
            ->find($id);
        return $media;
    }

    public function getPaginatedMedia($filters = []){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $paginator = $this->container->get('yarsha.service.pagination');
        if(array_key_exists('mode', $filters)){
            if($filters['mode'] == 'grid'){
                return $paginator->getPagerFanta(
                    $em->getRepository(Media::class)->getAllMediaQuery($filters)
                )->setMaxPerPage(40);
            }
        }
        return $paginator->getPagerFanta(
            $em->getRepository(Media::class)->getAllMediaQuery($filters)
        );
    }

    public function getAllMedia($filters = []){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $qb = $em->getRepository(Media::class)->getAllMediaQuery($filters);
        return $qb->getQuery()->getResult();
    }

    public function getMediaForGallery(Gallery $gallery){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $medias = $em->createQueryBuilder()
            ->select('m.id')
            ->from(Gallery::class, 'g')
            ->join('g.medias', 'm')
            ->where('g.id = :gallery')->setParameter('gallery', $gallery)
            ->getQuery()
            ->getScalarResult();
        $ids = [];
        foreach ($medias as $m){
            $ids[] = $m['id'];
        }
        $qb = $em->createQueryBuilder();
        if(!empty($ids)){
            $qb->select('m')
                ->from(Media::class, 'm')
                ->where(
                    $qb->expr()->notIn('m.id', $ids)
                );
        }   else    {
            $qb->select('m')
                ->from(Media::class, 'm');
        }

        return $qb->getQuery()->getResult();
    }

    public function getGalleryById($id){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $gallery = $em->getRepository(Gallery::class)->find($id);
        return $gallery;
    }

    public function mediaIsImage(Media $media){
        $qb = $this->container->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $qb->select('m')
            ->from(Media::class, 'm')
            ->where('m.id = :id')->setParameter('id', $media)
            ->andWhere(
                $qb->expr()->like('m.fileType', $qb->expr()->literal('%image'))
            );
        $result = $qb->getQuery()->getResult();
        if($result){
            return true;
        }   else    {
            return false;
        }
    }

    public function getFileUploadMaxSize() {
        static $max_size = -1;

        if ($max_size < 0) {
            $post_max_size = $this->parseSize(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            $upload_max = $this->parseSize(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }

    public function parseSize($size) {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }


    public function getPostById($id){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $post = $em->getRepository(Post::class)->find($id);
        return $post;
    }


    public function getAllMediaForPost($filter){

        $em = $this->container->get('doctrine.orm.entity_manager');
        $qb = $em->createQueryBuilder();
        $qb->select('m')
            ->from(Media::class, 'm');
        $qb->andWhere(
            $qb->expr()->like('m.fileType', $qb->expr()->literal('%image%'))
        );

        if(array_key_exists('title', $filter)){
            if(!empty($filter['title'])){
                $qb->andWhere(
                    $qb->expr()->like('m.title', $qb->expr()->literal('%'.$filter['title'].'%'))
                );
            }
        }


        $qb->orderBy('m.id','DESC');

        return $qb->getQuery()->getResult();

    }

    public function saveThumbnail(Media $media, $width = 150, $height = 150){
        if(strpos($media->getFile()->getClientMimeType(), 'image') !== false){
            $uploadDir = $this->container->getParameter('upload_dir');
            $thumbnailDir = $this->container->getParameter('thumbnail_dir');

            $media->getDimensions();

            $image = new Image(['driver' => 'gd']);
            if(!is_dir($this->container->getParameter('thumbnail_dir'))){
                mkdir($this->container->getParameter('thumbnail_dir'));
            }
            $thumbnail = $image
                ->make($uploadDir.'/'.$media->getFilename())
                ->resize($width, $height);
            $thumbnail->save(
                $thumbnailDir.'/thumbnail_'.$media->getFilename(),
                50
            );
            $thumbnail_url = $this->container->get('assets.packages')->getUrl($thumbnailDir.'/thumbnail_'.$media->getFilename());
            $media->setThumbnailUrl($thumbnail_url);
        }
    }

    public function createThumbnail(Media $media, $newWidth = 150, $newHeight = 150)
    {
        $uploadDir = $this->container->getParameter('upload_dir');
        $imageName = $media->getFilename();
        $path = $uploadDir . '/' . $imageName;

        $mime = getimagesize($path);

        if($mime['mime']=='image/png') {
            $sourceImage = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $sourceImage = imagecreatefromjpeg($path);
        }

        $old_x          =   imageSX($sourceImage);
        $old_y          =   imageSY($sourceImage);

        if($old_x > $old_y)
        {
            $thumb_w    =   $newWidth;
            $thumb_h    =   $old_y*($newHeight/$old_x);
        }

        if($old_x < $old_y)
        {
            $thumb_w    =   $old_x*($newWidth/$old_y);
            $thumb_h    =   $newHeight;
        }

        if($old_x == $old_y)
        {
            $thumb_w    =   $newWidth;
            $thumb_h    =   $newHeight;
        }

        $imageThumb =   ImageCreateTrueColor($thumb_w,$thumb_h);

        imagecopyresampled($imageThumb,$sourceImage,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

        // New save location
        $thumbnailDir = $this->container->getParameter('thumbnail_dir') .'/thumbnail_'. $imageName;

        if($mime['mime']=='image/png') {
            $result = imagepng($imageThumb,$thumbnailDir,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $result = imagejpeg($imageThumb,$thumbnailDir,80);
        }

        $thumbnail_url = $this->container->get('assets.packages')->getUrl($thumbnailDir);
        $media->setThumbnailUrl($thumbnail_url);

        imagedestroy($imageThumb);
        imagedestroy($sourceImage);

//        return $result;
    }

    public function makeThumbnail(Media $media, $thumbWidth = 150, $thumbHeight = 150)
    {
        if(strpos($media->getFile()->getClientMimeType(), 'image') !== false){
            $uploadDir = $this->container->getParameter('upload_dir');
            $imageName = $media->getFilename();
            $path = $uploadDir . '/' . $imageName;
            $thumbPath = $this->container->getParameter('thumbnail_dir') .'/thumbnail_'. $imageName;

            $mime = getimagesize($path);
            $sourceImage = null;

            if($mime['mime']=='image/png') {
                $sourceImage = imagecreatefrompng($path);
            }
            if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
                $sourceImage = imagecreatefromjpeg($path);
            }

            if (!$sourceImage) {
                echo "ERROR:could not create image handle ". $imageName;
                exit( 0 );
            }

            $width = imageSX($sourceImage);
            $height = imageSY($sourceImage);

            $image = new Image(['driver' => 'gd']);
            if(!is_dir($this->container->getParameter('thumbnail_dir'))){
                mkdir($this->container->getParameter('thumbnail_dir'));
            }

            if($width > $height)
            {
                $thumbnail  = $image->make($path)
                    ->heighten($thumbHeight)
//                    ->widen($thumbWidth)
                ;
                $thumbnail->save($thumbPath, 100);
            }

            $thumbnail_url = $this->container->get('assets.packages')->getUrl($thumbPath);
            $media->setThumbnailUrl($thumbnail_url);
        }

    }

    public function generateThumbnail(Media $media, $thumbWidth = 150, $thumbHeight = 150)
    {
        $uploadDir = $this->container->getParameter('upload_dir');
        $imageName = $media->getFilename();
        $path = $uploadDir . '/' . $imageName;

        $mime = getimagesize($path);
        $sourceImage = null;

        if($mime['mime']=='image/png') {
            $sourceImage = imagecreatefrompng($path);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $sourceImage = imagecreatefromjpeg($path);
        }

        if (!$sourceImage) {
            echo "ERROR:could not create image handle ". $imageName;
            exit( 0 );
        }

        $width = imageSX($sourceImage);
        $height = imageSY($sourceImage);

        if (!$width || !$height) {
            echo "ERROR:Invalid width or height";
            exit(0);
        }

        // Portrait or landscape image?
        if($width > $height) {
            $image_mode = 'landscape';
        }
        elseif($width < $height) {
            $image_mode = 'portrait';
        }
        else {
            $image_mode = 'landscape';
        }

        $targetThumbRatio = $thumbWidth / $thumbHeight;
        $sourceImageRatio = $width / $height;

        // calculate zwarte balken, onder of boven en hoe groot
        if($image_mode == 'portrait') {

            if($sourceImageRatio < $targetThumbRatio) {
                $resize_thumb_ratio = $thumbHeight / $height;
                $new_thumb_width = $width * $resize_thumb_ratio;
                $blackness = $thumbWidth - $new_thumb_width;
                $left_thumb_start = $blackness/2;
                $leftthumbstart = round($left_thumb_start);
                $topthumbstart = 0;
                $thumbWidth = $new_thumb_width;
            }
            else {
                $image_mode = 'landscape';
            }
        }

        if($image_mode == 'landscape') {
            // zwarte balken boven en onder
            if($sourceImageRatio > $targetThumbRatio) {
                $resize_thumb_ratio = $thumbWidth / $width;
                $new_thumb_height = $height * $resize_thumb_ratio;
                $blackness = $thumbHeight - $new_thumb_height;
                $top_thumb_start = $blackness/2;
                $topthumbstart = round($top_thumb_start);
                $leftthumbstart = 0;
                $thumbHeight = $new_thumb_height;
            }

        }

        // create 2 black (empty) images
        $new_thumb = ImageCreateTrueColor($thumbWidth, $thumbHeight);

        if (!@imagecopyresampled($new_thumb, $sourceImage, $leftthumbstart, $topthumbstart, 0, 0, $thumbWidth, $thumbHeight, $width, $height)) {
            echo "ERROR:Could not resize image";
            exit(0);
        }

        // New save location
        $thumbnailDir = $this->container->getParameter('thumbnail_dir') .'/thumbnail_'. $imageName;

        if($mime['mime']=='image/png') {
            $result = imagepng($new_thumb,$thumbnailDir,8);
        }
        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $result = imagejpeg($new_thumb,$thumbnailDir,80);
        }

        $thumbnail_url = $this->container->get('assets.packages')->getUrl($thumbnailDir);
        $media->setThumbnailUrl($thumbnail_url);

        imagedestroy($new_thumb);
        imagedestroy($sourceImage);

    }

    public function getImageDimensions($path){

        $mime = getimagesize($path);
        $sourceImage = null;

        if($mime['mime']=='image/png') {
            $sourceImage = imagecreatefrompng($path);
        }

        if($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            $sourceImage = imagecreatefromjpeg($path);
        }

        if (!$sourceImage) {
            throw new \Exception("ERROR:could not create image handle ");
        }

        return [imageSX($sourceImage), imageSY($sourceImage), $sourceImage, $mime];
    }

    public function getImageThumbCanvas($mime, $width, $height){
        if($mime['mime']=='image/png') {
            return imagecreatefrompng($width, $height);
        }elseif($mime['mime']=='image/jpg' || $mime['mime']=='image/jpeg' || $mime['mime']=='image/pjpeg') {
            return imagecreatefromjpeg($width, $height);
        }else{
            return imagecreate($width, $height);
        }

    }

    public function resizeImage(Media $media, $desiredWidth = 150, $desiredHeight = 150){

        if(strpos($media->getFile()->getClientMimeType(), 'image') !== false){
            $uploadDir = $this->container->getParameter('upload_dir');
            $imageName = $media->getFilename();
            $path = $uploadDir . '/' . $imageName;
            $thumbPath = $this->container->getParameter('thumbnail_dir') .'/thumbnail_'. $imageName;
            $dimensions = $this->getImageDimensions($path);

            $width = $dimensions[0];
            $height = $dimensions[1];
            $thumb = imagecreatetruecolor($desiredWidth, $desiredHeight);
            imagesavealpha($thumb, true);

            $isPng = $dimensions[3]['mime'] == 'image/png';

            $color = $isPng
                ? imagecolorallocatealpha($thumb, 0, 0, 0, 127)
                : imagecolorallocate($thumb, 255, 255, 255);

            imagefill($thumb, 0, 0, $color);

            if($width <= $desiredWidth and $height <= $desiredHeight)
            {
                $top = $height != $desiredHeight ? round(($desiredHeight - $height) / 2) : 0;
                $left = $width != $desiredWidth ? round(($desiredWidth - $width) / 2) : 0;
                $desiredWidth = $width;
                $desiredHeight = $height;
                imagecopyresampled($thumb, $dimensions[2], $left, $top, 0, 0, $desiredWidth, $desiredHeight, $width, $height);
            }

            if($height <= $desiredHeight and $width > $desiredWidth){
                $top = $height != $desiredHeight ? round(($desiredHeight - $height) / 2) : 0;
                $left = round(($desiredWidth - $width) / 2);
                $desiredHeight = $height;
                $desiredWidth = $width;
                imagecopyresampled($thumb, $dimensions[2], $left, $top, 0, 0, $desiredWidth, $desiredHeight, $width, $height);
            }

            if($width <= $desiredWidth and $height > $desiredHeight){
                $top = round(($desiredHeight - $height) / 2);
                $left = $width != $desiredWidth ? round(($desiredWidth - $width) / 2) : 0;
                $desiredWidth = $width;
                $desiredHeight = $height;
                imagecopyresampled($thumb, $dimensions[2], $left, $top, 0, 0, $desiredWidth, $desiredHeight, $width, $height);
            }

            if( $width > $desiredWidth && $height > $desiredHeight){

                $targetRatio = $desiredWidth / $desiredHeight;
                $imageRatio = $width / $height;

                if($imageRatio < $targetRatio) {
                    $thumbRatio = $desiredWidth / $width;
                    $thumbHeight = $desiredHeight;
                    $desiredHeight = $height * $thumbRatio;
                    $topthumbstart = round(($thumbHeight - $desiredHeight)/2);
                    $leftthumbstart = 0;
                }else{
                    $thumbRatio = $desiredHeight / $height;
                    $thumbWidth = $desiredWidth;
                    $desiredWidth = $width * $thumbRatio;
                    $topthumbstart = 0;
                    $leftthumbstart = round(($thumbWidth - $desiredWidth)/2);
                }

                imagecopyresampled($thumb, $dimensions[2], $leftthumbstart, $topthumbstart, 0, 0, $desiredWidth, $desiredHeight, $width, $height);
            }

            if($thumb)
            {
                $result = $isPng ? imagepng($thumb,$thumbPath,8) : imagejpeg($thumb,$thumbPath,80);
                $thumbnail_url = $this->container->get('assets.packages')->getUrl($thumbPath);
                $media->setThumbnailUrl($thumbnail_url);
                imagedestroy($thumb);
                imagedestroy($dimensions[2]);
            }
        }

    }


}