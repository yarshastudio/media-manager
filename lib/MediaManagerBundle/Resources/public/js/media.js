$('.addmedia').on('click', function () {
    var textareaObj = $(this).data('textarea');
    $('#insertMediaCkeditor').data('textarea', textareaObj);
});

$('#insertMediaCkeditor').on('click', function () {
    if ($(this).data('textarea') != null) {
        var textareaObj = $(this).data('textarea');
        $('.image-select').find('option:selected').first().each(function (index, item) {
            var hostname = window.location.hostname;
            var domain = document.URL;
            var parts = domain.split('//');
            var url = hostname != 'localhost' ?
                parts[0] +'//'+hostname+ $(item).attr("data-for-ckeditor") :
                $(item).attr("data-for-ckeditor");
            var alttext = $(item).attr('data-alttext');
            $.fn.insertAtCaret = function (myValue) {
                myValue = myValue.trim();
                CKEDITOR.instances[textareaObj].insertHtml(myValue);
            };

            $.fn.insertAtCaret("<img src='" + url + "' alt='"+alttext+"'/>");
        });
        $(this).data('textarea', null);
        $('#medialibrary').modal('hide');
    }

    return false;
});







