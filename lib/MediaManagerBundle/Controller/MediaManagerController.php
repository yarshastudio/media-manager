<?php

namespace Yarsha\MediaManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yarsha\MediaManagerBundle\Entity\Media;

class MediaManagerController extends Controller
{

    /**
     * @Route("/media/add", name="yarsha_media_manager_index")
     */
    public function indexAction(Request $request){
        return $this->render('YarshaMediaManagerBundle:Default:index.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/media/list", name="yarsha_media_manager_list_medias")
     */
    public function listMediaAction(Request $request){
        $mode = ($request->get('mode') == 'grid') ? 'grid' : 'list';

        $filters = $request->query->all();
        $mediaService = $this->get('yarsha.service.media_manager');
        $medias = $mediaService->getPaginatedMedia($filters);
        return $this->render('@YarshaMediaManager/media/list.html.twig', [
            'medias' => $medias,
            'mode' => $mode
        ]);
    }

}
