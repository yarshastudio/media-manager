<?php

namespace Yarsha\MediaManagerBundle\Controller;

use Intervention\Image\ImageManager;
use Ivory\CKEditorBundle\Exception\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Yarsha\MediaManagerBundle\Entity\Gallery;
use Yarsha\MediaManagerBundle\Entity\Media;
use Yarsha\MediaManagerBundle\Form\GalleryType;
use Yarsha\MediaManagerBundle\Form\MediaType;

class AjaxController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/file_upload", name="yarsha_media_manager_file_upload")
     */
    public function uploadAction(Request $request){
       $files = $request->files->all();
       $em = $this->getDoctrine()->getManager();
       $service = $this->get('yarsha.service.media_manager');
        $webDir = $this->get('kernel')->getRootDir().'/../web/';
        $uploadDir = $this->getParameter('upload_dir');
       foreach ($files as $file){
           if($file instanceof UploadedFile){
               $maxSize = $service->getFileUploadMaxSize();
               $filesize = $file->getClientSize();
               if($maxSize < $filesize or !$filesize or $filesize <= 0){
                   $maxSize = $maxSize / (1024 * 1024);
                    return new JsonResponse([
                        'success' => false,
                        'message' => "File too large. Maximum file size allowed is {$maxSize} MB."
                    ]);
               }

               if(strpos($file->getClientMimeType(), 'image') !== false){
                   $info = getimagesize($file);
                   $info = "{$info[0]} x {$info[1]}";
               }    else    {
                   $info = '';
               }

               $fileName = explode('.',$file->getClientOriginalName());
               $originalFileName = $fileName[0];
               $fileExtension = $fileName[1];


               $media = new Media();
               $media->setFilename($originalFileName.'_'.date('YmdHis').'.'.$fileExtension);
               $media->setFile($file);
               $media->setFileSize($filesize);
               $media->setFileType($file->getClientMimeType());
               $media->setTitle($file->getClientOriginalName());
               $media->setDimensions($info);
               $media->getFile()->move(
                   $webDir.$uploadDir,
                   $media->getFilename()

               );
               $url = $this->get('assets.packages')->getUrl($uploadDir.'/'.$media->getFilename());
               $media->setUrl($url);
//               $service->saveThumbnail($media);
               $service->resizeImage($media);
               $media->setCreatedAt(new \DateTime('now'));
               $em->persist($media);
               try{
                   $em->flush();
                   return new JsonResponse([
                        'success' => true,
                       'message' => 'File uploaded',
                       'image'=>$media->getFilename()
                   ]);
               }    catch (\Throwable $e){
                   return new JsonResponse([
                       'success' => false,
                       'message' => $e->getMessage()
                   ]);
               }
           }
       }
       return new JsonResponse(['success' => false, 'message' => 'Something went wrong.']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/media/{id}/update", name="yarsha_media_manager_ajax_media_update")
     */
    public function addAction(Request $request){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $media = $this->getDoctrine()->getManager()
            ->getRepository(Media::class)->find($id);
        if(!$media){
            $media = new Media();
        }
        $form = $this->createForm(MediaType::class, $media);
        $form->handleRequest($request);
        if($form->isSubmitted() and $form->isValid()){
            $media = $form->getData();
            $media->setUpdatedAt(new \DateTime('now'));
            $em->persist($media);
            try{
                $em->flush();
                $data['success'] = true;
                $data['message'] = 'Media Updated.';
            }   catch (\Throwable $e){
                $data['success'] = false;
                $data['message'] = $e->getMessage();
            }
        }   else    {
            $data['success'] = false;
            $data['message'] = "Something went wrong.";
        }

        return new JsonResponse($data);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/media/{id}/detail", name="yarsha_media_manager_media_detail")
     */
    public function getMediaDetialAction(Request $request){
        $id = $request->get('id');
        if(!$id){
            return new JsonResponse([
                'success' => false,
                'message' => 'Something went wrong.'
            ]);
        }

        $media = $this->getDoctrine()->getManager()
            ->getRepository(Media::class)->find($id);
        if(!$media){
            return new JsonResponse([
                'success' => false,
                'message' => 'Something went wrong.'
            ]);
        }

        $form = $this->createForm(MediaType::class,$media);
        $data['template'] = $this->renderView('@YarshaMediaManager/media/mediaDetail.html.twig', [
            'media' => $media,
            'form' => $form->createView()
        ]);
        $data['success'] = true;

        return new JsonResponse($data);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/render/media", name="yarsha_media_manager_render_media")
     */
    public function mediaListAction(Request $request){
        $mode = $request->get('mode');
        if(!$mode){
            $mode = 'list';
        }

        if($mode != 'list' and $mode != 'grid'){
            $mode = 'list';
        }
        $filters = $request->query->all();
        $mediaService = $this->get('yarsha.service.media_manager');
        $medias = $mediaService->getAllMedia($filters);
        $data['template'] = $this->renderView('@YarshaMediaManager/media/mediaListTmpl.html.twig', [
            'medias' => $medias,
            'mode' => $mode
        ]);
        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/media/{id}/delete", name="yarsha_media_manager_ajax_media_delete")
     */
    public function deleteMedia(Request $request){
        $id = $request->get('id');
        $mediaService = $this->get('yarsha.service.media_manager');
        $media = $mediaService->getMediaById($id);
        $em = $this->getDoctrine()->getManager();
        if($media){
            $webDir = $this->get('kernel')->getRootDir().'/../web/';
            $uploadDir = $this->getParameter('upload_dir');
            $thumbnailDir = $this->getParameter('thumbnail_dir');
            $media->removeFile($webDir.$uploadDir);
            $media->removeThumbnail($webDir.$thumbnailDir);
            $em->remove($media);
            try{
                $em->flush();
                $data['success'] = true;
                $data['message'] = "Media deleted.";
            }   catch (\Exception $e){
                $data['success'] = false;
                $data['message'] = $e->getMessage();
            }
        }   else    {
            $data['success'] = false;
            $data['message'] = "Something went wrong.";
        }

        return new JsonResponse($data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/gallery/add/render", name="yarsha_media_manager_ajax_gallery_add_render")
     * @Route("/ajax/gallery/{id}/render", name="yarsha_media_manager_ajax_gallery_edit_render")
     */
    public function renderGalleryAction(Request $request){
        $id = $request->get('id');
        $service = $this->get('yarsha.service.media_manager');
        if($id) {
            $gallery = $service->getGalleryById($id);
            if (!$gallery) {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Gallery does not exist.'
                ]);
            }
        }  else     {
            $gallery = new Gallery();
        }
        $data['form'] = $this->createForm(GalleryType::class, $gallery)->createView();
        $data['gallery'] = $gallery;
        $data['medias'] = $service->getAllMedia();
        $data['isUpdating'] = true;
        $template = $this->renderView('@YarshaMediaManager/gallery/galleryDetail.html.twig', $data);
        return new JsonResponse([
            'success' => true,
            'message' => 'Template rendered',
            'template' => $template
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/imagecrop/{id}/render", name="yarsha_media_manager_ajax_render_image_crop")
     */
    public function renderImageCropSectionAction(Request $request){
        $id = $request->get('id');
        if($id){
            $webDir = $this->get('kernel')->getRootDir().'/../web/';
            $uploadDir = $this->getParameter('upload_dir');
            $thumbnailDir = $this->getParameter('thumbnail_dir');
            $mediaService = $this->get('yarsha.service.media_manager');
            $media = $mediaService->getMediaById($id);
            $em = $this->getDoctrine()->getManager();
            if($media and $media instanceof Media){
                $files = $request->files->all();
                foreach ($files as $file){
                    if($file instanceof UploadedFile){
                        $maxSize = $mediaService->getFileUploadMaxSize();
                        $filesize = $file->getClientSize();
                        if($maxSize < $filesize or !$filesize or $filesize <= 0){
                            $maxSize = $maxSize / (1024 * 1024);
                            return new JsonResponse([
                                'success' => false,
                                'message' => "File too large. Maximum file size allowed is {$maxSize} MB."
                            ]);
                        }

                        if(strpos($file->getClientMimeType(), 'image') !== false){
                            $info = getimagesize($file);
                            $info = "{$info[0]} x {$info[1]}";
                        }    else    {
                            $info = '';
                        }
                        $filename = $media->getFilename();
                        $media->removeFile($webDir.$uploadDir);
                        $media->setFile($file);
                        $media->setFileSize($filesize);
                        $media->setDimensions($info);
                        $media->getFile()->move(
                            $webDir.$uploadDir,
                            $media->getFilename()
                        );
                        $url = $this->get('assets.packages')->getUrl($uploadDir.'/'.$filename);
                        $media->setUrl($url);
                        $mediaService->saveThumbnail($media);
                        $media->setUpdatedAt(new \DateTime('now'));
                        $em->persist($media);
                    }
                    try{
                        $em->flush();
                        $data = [
                            'success' => true,
                            'message' => 'Image cropped.'
                        ];
                    }   catch (\Throwable $e){
                        $data = [
                            'success' => false,
                            'message' => $e->getMessage()
                        ];
                    }
                }
            }   else    {
                return new JsonResponse([
                    'success' => false,
                    'message' => 'Media not found.'
                ]);
            }

            $data['media'] = $media;
            $response['template'] = $this->renderView('@YarshaMediaManager/ajax/imageCropSection.html.twig', $data);
            return new JsonResponse($response);
        }

        return new JsonResponse([
            'success' => false,
            'message' => 'Media not found.'
        ]);

    }

     /**
      *
      * @Route("/ajax/render/mediaimages", name="yarsha_media_manager_ajax_media_images")
      */
    public function renderAllMediaImagesAction(Request $request){

        $serachValue = $request->get('value');
        $filter = [];
        if(!empty($serachValue)){

            $filter['title'] = $serachValue;
        }else{
            $filter['title'] = '';
        }

        $service = $this->get('yarsha.service.media_manager');
        $content = $service->getAllMediaForPost($filter);

        $counter = 0;

        $media = [];
        foreach ($content as $c) {
            $media[$counter]['id'] = $c->getId();
            $media[$counter]['url'] = $c->getUrl();
            $media[$counter]['thumbnailUrl'] = $c->getThumbnailUrl();
            $media[$counter]['image'] = $c->getFilename();
            $media[$counter]['title'] = ($c->getTitle() != '') ? $c->getTitle() : '';
            $media[$counter]['description'] = ($c->getDescription() != "") ? $c->getDescription() : '';
            $media[$counter]['alttext'] = ($c->getAltName() != "") ? $c->getAltName() : '';
            $media[$counter]['caption'] = ($c->getCaption() != "") ? $c->getCaption() : '';
            $media[$counter]['created'] = ($c->getCreatedAt() != "") ? $c->getCreatedAt()->format('F j Y') : '';
            $media[$counter]['updated'] = ($c->getUpdatedAt() != "") ? $c->getUpdatedAt()->format('F j Y') : '';
            $counter++;
        }

        $response['media'] = $media;
        return new JsonResponse($response);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/image/{id}/detail", name="yarsha_media_manager_images_detail")
     */
    public function detailImageAction(Request $request){


        $id = $request->get('id');

        if($id){
            $service = $this->get('yarsha.service.media_manager');
            $detail = $service->getMediaById($id);
        }else{
            throw new NotFoundHttpException("Page not found");
        }

        $media = [
            "id" => $detail->getId(),
            "image" => $detail->getFilename(),
            "title" => $detail->getTitle(),
            "thumbnailUrl" => $detail->getThumbnailUrl(),
            "url" => $detail->getUrl(),
            "caption" => $detail->getCaption(),
            "description" => $detail->getDescription(),
            "alttext" => $detail->getAltName(),
            "created" => ($detail->getCreatedAt() != "") ? $detail->getCreatedAt()->format('F j Y') : '',
            "updated" => ($detail->getUpdatedAt() != "") ? $detail->getUpdatedAt()->format('F j Y') : ''
        ];


        $response['media'] = $media;
        return new JsonResponse($response);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/ajax/edit/{id}/detail", name="yarsha_media_manager_edit_images_detail")
     */
    public function imageDetailEditAction(Request $request){

        $id = $request->get('id');
        $name = $request->get('name');
        $val = $request->get('value');

        if($id){
            $service = $this->get('yarsha.service.media_manager');
            $detail = $service->getMediaById($id);
            $em = $this->get('doctrine.orm.entity_manager');
            if($name == 'alttext'){
                $detail->setAltName($val);
            }

            if($name == 'caption'){
                $detail->setCaption($val);
            }

            if($name == 'desc-img'){
                $detail->setDescription($val);
            }

            $em->persist($detail);

          try{
                $em->flush($detail);
                $response['status'] = true;
                $response['message'] = 'success';

          }catch (Exception $e){
              $response['status'] = false;
                $response['message'] = $e->getMessage();

          }
        }else{
            $response['status'] = false;
            $response['message'] = 'error';
        }
        return new JsonResponse($response);
    }


}
