<?php

namespace Yarsha\MediaManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Yarsha\MediaManagerBundle\Entity\Gallery;
use Yarsha\MediaManagerBundle\Form\GalleryType;

class GalleryController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/media/gallery", name="yarsha_media_manager_media_gallery")
     */
    public function listGalleryAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $gallery = $em->getRepository(Gallery::class)->findAll();
        return $this->render('@YarshaMediaManager/gallery/galleryList.html.twig', [
            'gallery' => $gallery
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/media/gallery/add", name="yarsha_media_manager_media_gallery_add")
     * @Route("/media/gallery/{id}/edit", name="yarsha_media_manager_media_gallery_edit")
     */
    public function addGallery(Request $request){
        $id = $request->get('id');
        $mediaService = $this->get('yarsha.service.media_manager');
        $em = $this->getDoctrine()->getManager();
        $data['medias'] = $mediaService->getAllMedia();
        if(!$id){
            $gallery = new Gallery();
            $data['isUpdating'] = false;
        }   else {
            $data['isUpdating'] = true;
            $gallery = $mediaService->getGalleryById($id);
            if(!$gallery){
                return $this->redirectToRoute('yarsha_media_manager_media_gallery');
            }
        }

        $form = $this->createForm(GalleryType::class, $gallery);
        $form->handleRequest($request);
        if($form->isValid() and $form->isSubmitted()){
            $gallery = $form->getData();
            if($data['isUpdating']){
                $gallery->setUpdatedAt(new \DateTime('now'));
            }   else    {
                $gallery->setCreatedAt(new \DateTime('now'));
            }
            $em->persist($gallery);
            try{
                $em->flush();
                if($data['isUpdating']){
                    $this->addFlash('success', 'Gallery updated.');
                }   else    {
                    $this->addFlash('success', 'Gallery added.');
                }
            }   catch (\Throwable $e){
                $this->addFlash('error', 'Something went wrong. Unable to add gallery.');
            }
            return $this->redirectToRoute('yarsha_media_manager_media_gallery');
        }
        $data['form'] = $form->createView();
        return $this->render('@YarshaMediaManager/gallery/addGallery.html.twig', $data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/media/gallery/{id}/delete", name="yarsha_media_manager_media_gallery_delete")
     */
    public function deleteGalleryAction(Request $request){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $service = $this->get('yarsha.service.media_manager');
        $gallery = $service->getGalleryById($id);
        if(!$gallery){
            return $this->redirectToRoute('yarsha_media_manager_media_gallery');
        }
        $em->remove($gallery);
        try{
            $em->flush();
            $this->addFlash('success', 'Gallery deleted.');
        }   catch (\Throwable $e){
            $this->addFlash('error', $e->getMessage());
        }
        return $this->redirectToRoute('yarsha_media_manager_media_gallery');
    }

}
