<?php

namespace Yarsha\MediaManagerBundle\Twig;

use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Yarsha\MediaManagerBundle\Entity\Media;

class MediaManagerExtension extends \Twig_Extension{

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('render_media_date_filter', [
                $this,
                'renderMediaDateFilter'
            ]),
            new \Twig_SimpleFunction('render_media_menu', [
                $this,
                'renderMediaManagerMenu'
            ], [
                'needs_environment' => true,
                'is_safe' => ['html']
            ]),
            new \Twig_SimpleFunction('render_media_filter_bar', [
                $this,
                'renderMediaFilterBar'
            ], [
                'needs_environment' => true,
                'is_safe' => ['html']
            ]),
            new \Twig_SimpleFunction('render_add_media_button', [
                $this,
                'renderAddMediaButton'
            ])
        );
    }

    public function renderMediaDateFilter($class = '', $id = 'filterTypeDate'){
        $service = $this->container->get('yarsha.service.media_manager');
        $medias = $service->getAllMedia();
        $dates = [];
        $uniqueDates = [];
        foreach ($medias as $media){
            if($media instanceof Media){
                $date = $media->getCreatedAt();
                if($date instanceof \DateTime){
                    $dates[] = [
                        'label' => $date->format('F , Y'),
                        'value' => $date->format('Y-m')
                    ];
                }
            }
        }

        foreach ($dates as $date) {
            $uniqueDates[$date['value']] = $date;
        }

        $html = "<select class='{$class}' id='{$id}'><option value='all'>All dates</option>";
        foreach($uniqueDates as $d){
            $html .= "<option value='{$d['value']}'>{$d['label']}</option>";
        }
        $html .= "</select>";
        return $html;
    }

    public function renderMediaManagerMenu(\Twig_Environment $environment){
        return $environment->render('@YarshaMediaManager/includes/navbar.html.twig');
    }

    public function renderMediaFilterBar(\Twig_Environment $environment){
        return $environment->render('@YarshaMediaManager/includes/mediafilter.html.twig');
    }

    public function renderAddMediaButton(
        $id = 'addmedia',
        $class = 'addmedia',
        $dataTarget = '#medialibrary',
        $dataTextArea = 'yarsha_post_description'
    ){
        $id = ($id == null or $id == '') ? 'addmedia' : $id;
        $class = ($class == null or $class == '') ? 'addmedia' : $class;
        $dataTarget = ($dataTarget == null or $dataTarget == '') ? '#medialibrary' : $dataTarget;
        $dataTextArea = ($dataTextArea == null or $dataTextArea == '') ? 'yarsha_post_description' : $dataTextArea;

        $html = "<a href=''
           data-textarea='{$dataTextArea}'
           class='btn btn-info btn-sm {$class}'
           id='{$id}'
           data-target='{$dataTarget}'
           data-toggle='modal'>Add Media</a>";
        return $html;
    }

}