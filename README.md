**Basic Usage**
<p>In your composer.json file add </p>

```
 "require" : {
         "yarsha/mediamanager": "dev"
 }      
```
<p>Add our gitlab repo to the project</p>

```
"repositories": [
        {
            "type": "git",
            "url": "git@gitlab.com:yarshastudio/media-manager.git"
        }
    ]
```

<p>Run composer update</p>

**Configure upload directory**
<p>In parameters.yml file add</p> 
<p>upload_dir: "uploads/medias"</p>
<p>thumbnail_dir: "uploads/medias/thumbnails"</p>

**under app/config/routing.yml**
````
yarsha_media_manager:
    resource: "@YarshaMediaManagerBundle/Controller/"
    type: annotation

elfinder:
    resource: "@FMElfinderBundle/Resources/config/routing.yml"
````

**Register bundles**
<p>In appKernel.php</p>

````
new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
new FM\ElfinderBundle\FMElfinderBundle(),
new Yarsha\MediaManagerBundle\YarshaMediaManagerBundle()
````

**Install required assets**
<p>Run php bin/console assets:install</p>

**Mandatory assets**
```
<link rel="stylesheet" href="{{ asset('bundles/yarshamediamanager/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('bundles/yarshamediamanager/css/basic.min.css') }}">
<link rel="stylesheet" href="{{ asset('bundles/yarshamediamanager/css/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ asset('bundles/yarshamediamanager/css/style.css') }}">

<script src="{{ asset('bundles/yarshamediamanager/js/jquery.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/dropzone.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/notify.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/bootbox.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/customScripts.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/image-picker.min.js') }}"></script>
<script src="{{ asset('bundles/yarshamediamanager/js/media.js') }}"></script>
{% include '@YarshaMediaManager/includes/scripts.html.twig' %}
```

**Further configurations**
<p>
Configure ckeditor and el_finder from
<ul>
    <li>
        <a href="https://symfony.com/doc/master/bundles/IvoryCKEditorBundle/installation.html">
            Ivory CKEditor
        </a>
    </li>
    <li>
        <a href="https://github.com/helios-ag/FMElfinderBundle">El finder</a>
    </li>
</ul>
</p>